<?php

namespace Drupal\cmrf_form_processor_mollie\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\mollie\Events\MollieNotificationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Mollie_ke event subscriber.
 */
class MollieFormProcessorSubscriber implements EventSubscriberInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function callFormProcessor(MollieNotificationEvent $event): void {

    $httpCode = 200;

    // Fetch the transaction.
    try {
      $submission = $this->entityTypeManager->getStorage('webform_submission')
        ->load($event->getContextId());
      $webform = $submission->getWebform();
      $handlers = array_filter($webform->getHandlers(), function($handler) {
        $plugin_definition = $handler->getPluginDefinition();
        return $plugin_definition['provider'] === 'cmrf_form_processor';
      });
      $transaction = $this->entityTypeManager->getStorage('mollie_payment')
        ->load($event->getTransactionId());

      foreach ($handlers as $handler) {
        if ($handler->checkConditions($submission)) {
          $handler->postSave($submission, TRUE, [
            'mollie_payment_id' => $event->getTransactionId(),
            'mollie_payment_status' => $transaction->getStatus(),
          ]);
        }
      }
    }
    catch (\Exception $e) {
      /* the code below enables throwing writing the log for versions of Drupal lower than 10.1.0
         probably not needed, but a good example how to write multi version drupal code
      */
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(
        \Drupal::VERSION,
        '10.1.0',
        fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('cmrf_form_processor_mollie'), $e),
        fn() => watchdog_exception('cmrf_form_processor_mollie', $e));
      $httpCode = 500;
    }

    // Set the HTTP code to return to Mollie.
    $event->setHttpCode($httpCode);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      MollieNotificationEvent::EVENT_NAME => 'callFormProcessor',
    ];
  }

}

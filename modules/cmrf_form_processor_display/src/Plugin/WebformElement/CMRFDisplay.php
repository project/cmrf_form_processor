<?php

namespace Drupal\cmrf_form_processor_display\Plugin\WebformElement;

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 16-Dec-2022
 * @license  GPL-2.0-or-later
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformMarkupBase;

/**
 * Provides a CMRF Display webform element.
 *
 * @WebformElement(
 *   id = "cmrf_display",
 *   label = @Translation("CMRF Display"),
 *   description = @Translation("Display only"),
 *   category = @Translation("CMRF"),
 * )
 */
class CMRFDisplay extends WebformMarkupBase {

  /**
   * Defines the default properties for the CMRF Display element.
   *
   * @return array
   *   An array of default properties.
   */
  protected function defineDefaultProperties() {
    return [
      'wrapper_attributes' => [],
      'cmrf_display_mode' => 'literal',
      'cmrf_display_text' => '',
      'cmrf_display_atts' => '',
      // Required for the element to be populated with the default value.
      'value' => '',
    ] + parent::defineDefaultProperties();
  }

  public function isInput(array $element) {
    // Must be TRUE for the element to be populated with the default value.
    return TRUE;
  }

  /**
   * Builds the form for configuring the CMRF Display element.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The updated form array.
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['cmrf_display'] = [
      '#type' => 'fieldset',
      '#title' => $this->t("CMRF Display settings"),
      'cmrf_display_mode' => [
        '#type' => 'radios',
        '#title' => $this->t('Mode'),
        '#options' => [
          'literal' => $this->t('Literal'),
          'link' => $this->t('Link'),
          'button' => $this->t('Button'),
        ],
      ],
      'cmrf_display_text' => [
        '#type' => 'textfield',
        '#title' => $this->t('Link or Button Text'),
        '#states' => [
          'visible' => [
            ":input[name='properties[cmrf_display_mode]']" => [
              ['value' => 'link'],
              ['value' => 'button'],
            ],
          ],
        ],
      ],
      'cmrf_display_atts' => [
        '#type' => 'textfield',
        '#title' => $this->t('Attributs to style the button'),
        '#states' => [
          'visible' => [
            ":input[name='properties[cmrf_display_mode]']" => ['value' => 'button'],
          ],
        ],
      ],
    ];
    return $form;
  }

}

<?php

namespace Drupal\cmrf_form_processor_display\Element;

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 16-Dec-2022
 * @license  GPL-2.0-or-later
 */

use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a lookup using CiviMRF.
 *
 * @FormElement("cmrf_display")
 */
class CMRFDisplay extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => FALSE,
      '#pre_render' => [
        [$class, 'preRenderCmrfDisplay'],
      ],
    ];
  }

  /**
   * Pre-renders the CMRF display element.
   *
   * @param array $element
   *   The element to pre-render.
   *
   * @return array
   *   The pre-rendered element.
   */
  public static function preRenderCmrfDisplay(array $element) {
    $value = $element['#value'] ?? '';
    switch ($element['#cmrf_display_mode'] ?? NULL) {
      case 'link':
        $element['#markup'] = t('<a target="_blank" href=":url">:text</a>.', [
          ':url' => $value,
          ':text' => $element['#cmrf_display_text'],
        ]);
        break;

      case 'button':
        $element['#markup'] = t('<a class=":atts" target="_blank" href=":url">:text</a>.', [
          ':atts' => $element['#cmrf_display_atts'],
          ':url' => $value,
          ':text' => $element['#cmrf_display_text'],
        ]);
        break;

      default:
        $element['#markup'] = $value;
        break;
    }

    return $element;
  }

}

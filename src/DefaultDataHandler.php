<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor;

use \Drupal;
use Drupal\cmrf_form_processor\Plugin\WebformHandler\FormProcessorWebformHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultDataHandler extends FormProcessorBaseHandler {

  /**
   * @var array
   */
  protected $defaultValues = [];

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var array
   */
  protected $triggerDefaultElementKeys = [];

  public function __construct(ContainerInterface $container, Factory $factory, FormProcessorWebformHandler $webformHandler) {
    parent::__construct($container, $factory, $webformHandler);
    $this->triggerDefaultElementKeys = [];
    if (isset($this->configuration['form_processor_trigger_defaults']) && is_array($this->configuration['form_processor_trigger_defaults'])) {
      foreach($this->configuration['form_processor_trigger_defaults'] as $trigger => $checked) {
        if ($checked) {
          $this->triggerDefaultElementKeys[] = $trigger;
        }
      }
    }
  }

  /**
   * Prepare the form.
   * If retrieval of defaults is enabled we should retrieve default values
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param $operation
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function prepareForm(WebformSubmissionInterface $webform_submission, $operation, FormStateInterface $form_state) {
    if (empty($form_state->getUserInput()) && !empty($this->configuration['form_processor_enable_default'])) {
      // If a form has no user input it is rendered for the first time
      // then possible default values are retrieved from civicrm to
      // change this form from an insert to an update form.
      // Does the form have user import (because it is validating for example)
      // no default values must be retrieved because they would overwrite the user input
      $values = $this->formProcessorDefaultsDefault();
      if (!empty($values) && empty($values['is_error'])) {
        $data = [];
        unset($values['is_error']);
        foreach ($values as $field => $value) {
          $existingValue = $webform_submission->getElementData($field);
          // Keep existing webform submission values. And only add newly retrieved values
          if ($existingValue !== NULL && $existingValue !== '') {
            $data[$field] = $existingValue;
          } else {
            $data[$field] = $value;
          }
        }
        $webform_submission->setData($data);
      }
      elseif ($this->configuration['form_processor_enable_default'] == 'enabled_and_page_not_found_on_no_data') {
        throw new NotFoundHttpException();
      }
      elseif ($this->configuration['form_processor_enable_default'] == 'enabled_and_access_denied_on_no_data') {
        throw new AccessDeniedHttpException();
      }
    }
  }

  /**
   * Alter the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs alterations on the form array.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    if (!empty($this->configuration['form_processor_enable_default']) && $this->isMultiPageForm($form_state) && $form_state->isSubmitted()) {
      $currentPageNumber = $this->getPageNumber($form_state);
      $triggerElement = $form_state->getTriggeringElement();
      $isPreviousButton = false;
      if (in_array('::previous', $triggerElement['#submit'])) {
        $isPreviousButton = true;
      }
      if ($currentPageNumber >= 1 && !$isPreviousButton) {
        $alreadySubmittedElements = $this->getSubmittedElements($webform_submission, $form_state, TRUE, FALSE);
        $this->retrieveAndSetDefaultValues($form, $form_state, $webform_submission, $alreadySubmittedElements);
      }
    } elseif (count($this->triggerDefaultElementKeys)) {
      $wrapperId = $this->getWrapperId($form_state);
      $this->addDynamicDataRetrievalFields($form, $wrapperId, 'defaults', $this->triggerDefaultElementKeys, t('Retrieving data'));
      if ($form_state->isSubmitted() && $form_state->isRebuilding()) {
        $triggerElement = $form_state->getTriggeringElement();
        if (isset($triggerElement['#name']) && $triggerElement['#name'] == 'cmrf_form_processor_defaults_update_button') {
          $this->retrieveAndSetDefaultValues($form, $form_state, $webform_submission, []);
        }
      }
    }
  }

  /**
   * @return array
   */
  public function getDefaultValues(): array {
    if (empty($this->defaultValues)) {
      $this->defaultValues = $this->formProcessorDefaultsDefault();
    }
    return $this->defaultValues;
  }

  /**
   * Get the default values for the form processor.
   *
   * @return array
   *   The default values for the form processor.
   */
  public function formProcessorDefaultsFromSubmission(FormStateInterface $form_state, WebformSubmissionInterface $webformSubmission): array {
    $this->updateWebformSubmissionWithSubmittedValues($webformSubmission, $form_state);
    $params = $this->webformSubmissionToApiParams($webformSubmission, $form_state);
    return $this->factory->api($this->getConnection(), 'FormProcessorDefaults', $this->getFormProcessor(), $params, []);
  }

  /**
   * Get the default values for the form processor.
   *
   * @return array
   *   The default values for the form processor.
   */
  public function formProcessorDefaultsDefault(): array {
    $params = $this->getFormProcessorSubmissionParams();
    return $this->factory->api($this->getConnection(), 'FormProcessorDefaults', $this->getFormProcessor(), $params, []);
  }

  /**
   * Returns an array with default params.
   *
   * @return array
   */
  public function getFormProcessorDefaultParams(): array {
    return $this->getFormProcessorSubmissionParams();
  }

  /**
   * Retrieve and set default values.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @param WebformSubmissionInterface $webform_submission
   * @param array $ignoreFields
   * @return void
   */
  protected function retrieveAndSetDefaultValues(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission, array $ignoreFields) {
    $values = $this->formProcessorDefaultsFromSubmission($form_state, $webform_submission);
    if (!empty($values) && empty($values['is_error'])) {
      unset($values['is_error']);
      foreach ($values as $field => $value) {
        if (!in_array($field,$ignoreFields)) {
          $this->setElementValue($form, $form_state, $webform_submission, $field, $value);
        }
      }
    }
  }

}

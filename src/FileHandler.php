<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor;

use \Drupal;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Symfony\Component\Mime\MimeTypes;

class FileHandler {

  static $instance;

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  private function __construct() {
    $this->fileSystem = \Drupal::service('file_system');
  }

  public static function getInstance(): FileHandler {
    if (!self::$instance) {
      self::$instance = new FileHandler();
    }
    return self::$instance;
  }

  public function createFileFromUrl(string $url, string $subPath):? int {
    $fileDestination = 'private://cmrf_form_processor/' . Drupal::currentUser()->id() . '/' . $subPath . '/';
    if ($this->fileSystem->prepareDirectory($fileDestination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $baseName = md5($this->fileSystem->basename($url));
      $fileName = $this->fileSystem->createFilename($baseName, $fileDestination);
      $uri = $this->fileSystem->saveData(file_get_contents($url), $fileName);
      $file = File::create(['uri' => $uri]);
      $file->setOwnerId(Drupal::currentUser()->id());
      $file->setTemporary();
      $mimeTypes = MimeTypes::getDefault();
      $mimeType = $mimeTypes->guessMimeType($uri);
      if ($mimeType) {
        $file->setMimeType($mimeType);
        $extensions = $mimeTypes->getExtensions($mimeType);
        if (is_array($extensions) && count($extensions)) {
          $file->setFilename($baseName . '.' . reset($extensions));
        }
      }
      $file->save();
      return $file->id();
    }
    return null;
  }

}

<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor;

use CMRF\Core\Call;
use Drupal\cmrf_core\Core;
use Drupal\cmrf_form_processor\Plugin\WebformHandler\OptionsSet;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

class Factory {
  use DependencySerializationTrait;

  /**
   * The CiviMRF Core.
   *
   * @var \Drupal\cmrf_core\Core
   */
  protected $core;

  protected $defaultCache = '1 hour';

  public function __construct(Core $core) {
    $this->core = $core;
  }

  public function getDefaultCache() {
    return $this->defaultCache;
  }

  public function getConnectionTitle(string $connection): string {
    $connections = $this->core->getConnectors();
    if (isset($connections[$connection])) {
      return $connections[$connection];
    }
    return t('Unknown connection');
  }

  public function getFormProcessorTitle(string $formProcessor, string $connection): string {
    $formProcessors = $this->formProcessorList($connection);
    if (isset($formProcessors[$formProcessor])) {
      return $formProcessors[$formProcessor];
    }
    return t('Unknown form processor');
  }

  /**
   * @return array
   */
  public function getConnectors(): array {
    return $this->core->getConnectors();
  }


  /**
   * Get the list of form processors for the given connection.
   *
   * @param string $connection
   *   The connection for which to retrieve the form processors.
   *
   * @return array
   *   The list of form processors.
   */
  public function formProcessorList($connection) {
    $reply = $this->api($connection, 'FormProcessorInstance', 'list', [], ['limit' => 0, 'cache' => $this->defaultCache]);
    $values = [];
    if (is_array($reply) && isset($reply['values'])) {
      $values = $reply['values'];
    }
    return array_map(function ($value) {
      return $value['title'];
    }, $values);
  }

  /**
   * Get the fields for the form processor.
   *
   * @param string $connection
   *   The connection for the form processor.
   * @param string $formprocessor
   *   The form processor.
   * @param array $extraParams
   * @param string $metaDataCacheTimeout
   *   The metadata cache timeout
   *
   * @return array
   *   The fields for the form processor.
   */
  public function formProcessorFields($connection, $formprocessor, $extraParams = [], $metaDataCacheTimeout='') {
    if (empty($metaDataCacheTimeout)) {
      $metaDataCacheTimeout = $this->getDefaultCache();
    }
    $params['api_action'] = $formprocessor;
    $params = array_merge($params, $extraParams);
    $reply = $this->api($connection, 'FormProcessor', 'getfields',$params, ['limit' => 0, 'cache' => $metaDataCacheTimeout]);
    $fields = [];
    if (is_array($reply) && isset($reply['values'])) {
      $fields = $reply['values'];
    }
    return $fields;
  }

  /**
   * Get the fields available upon which a calculation can be triggered.
   *
   * @param string $connection
   * @param string $formProcessor
   * @param array $extraParams
   * @param string $metaDataCacheTimeout
   *    The metadata cache timeout
   *
   * @return array
   *   The name of the inputs upon which a calculation can be triggered.
   */
  public function formProcessorCalculationFields(string $connection, string $formProcessor, $extraParams = [], string $metaDataCacheTimeout = ''): array {
    if (empty($metaDataCacheTimeout)) {
      $metaDataCacheTimeout = $this->getDefaultCache();
    }
    $params['api_action'] = $formProcessor;
    $params = array_merge($params, $extraParams);
    $result = $this->api($connection, 'FormProcessorInstance', 'get_calculation_triggers', $params, ['limit' => 0, 'cache' => $metaDataCacheTimeout], []);
    if (empty($result['is_error']) && isset($result['values']) && is_array($result['values'])) {
      return $result['values'];
    }
    return [];
  }

  /**
   * Get the default values for the form processor.
   *
   * @param string $connection
   *   The connection for the form processor.
   * @param string $formprocessor
   *   The form processor.
   * @param string $metaDataCacheTimeout
   *
   * @return array
   *   The default values for the form processor.
   */
  public function formProcessorDefaultsParams(string $connection, string $formprocessor, string $metaDataCacheTimeout): array {
    $result = $this->api($connection, 'FormProcessorDefaults', 'getfields', ['api_action' => $formprocessor], ['limit' => 0, 'cache' => $metaDataCacheTimeout], []);
    if (!empty($result['is_error']) || (isset($result['count']) && $result['count'] == 0)) {
      return [];
    }
    else {
      return array_map(function ($value) {
        return $value['name'];
      }, $result['values']);
    }
  }

  /**
   * Get the outputs of the form processor.
   *
   * @param string $connection
   *   The connection for the form processor.
   * @param string $formprocessor
   *   The form processor.
   * @param array $extraParams
   * @param string $metaDataCacheTimeout
   *   The metadata cache timeout
   *
   * @return array
   *   The fields for the form processor.
   */
  public function formProcessorOutputs($connection, $formprocessor, $extraParams = [], $metaDataCacheTimeout=''): array {
    if (empty($metaDataCacheTimeout)) {
      $metaDataCacheTimeout = $this->getDefaultCache();
    }
    $params['form_processor_name'] = $formprocessor;
    $params = array_merge($params, $extraParams);
    $reply = $this->api($connection, 'FormProcessorInstance', 'getoutput',$params, ['limit' => 0, 'cache' => $metaDataCacheTimeout]);
    $fields = [];
    if (is_array($reply) && isset($reply['values'])) {
      $fields = $reply['values'];
    }
    return $fields;
  }

  /**
   * @param string $connection
   * @param string $entuty
   * @param string $action
   * @param array $params
   * @param array $options
   *
   * @return string[]
   */
  public function api(string $connection, string $entuty, string $action, array $params, array $options): array {
    try {
      $call = $this->core->createCall($connection, $entuty, $action, $params, $options);
      $result = $this->core->executeCall($call);
      if ($result instanceof \Drupal\cmrf_core\Call) {
        $result = $result->getReply();
      }
      if ($result == NULL) {
        return ['is_error' => '1'];
      }
    } catch (\Throwable $e) {
      return ['is_error' => '1'];
    }
    return $result;
  }

}

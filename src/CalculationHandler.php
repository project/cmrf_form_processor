<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

class CalculationHandler extends FormProcessorBaseHandler {

  /**
   * @var null|array
   */
  protected $calculationReply = null;

  /**
   * @var null|array
   */
  protected $calculationFields = null;

  protected $usedCalculationTokens = [];

  protected $tempUsedCalculationTokens = [];

  public function getCalculatedData(WebformSubmissionInterface $webformSubmission, FormStateInterface $form_state = null, bool $force = false): array {
    if (empty($this->calculationReply) || $force) {
      $params = $this->webformSubmissionToApiParams($webformSubmission, $form_state);
      $this->calculationReply = $this->factory->api(
        $this->getConnection(),
        'FormProcessorCalculation',
        $this->getFormProcessor(),
        $params,
        []
      );
    }
    return $this->calculationReply;
  }

  /**
   * Alter the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs alterations on the form array.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $wrapperId = $this->getWrapperId($form_state);
    if ($this->webformHandler->isCalculationEnabled()) {
      $defaultParams = $this->getFormProcessorSubmissionParams();
      $outputs = $this->getCalculationOutputs($defaultParams);
      $element_keys = $this->getCalculationParams($defaultParams);
      if (count($element_keys) && $form_state->isSubmitted()) {
        $this->doCalculation($form, $form_state, $webform_submission);
      }
      if (count($outputs) && $this->areOutputsOnThisPage($outputs, $form_state, $webform_submission)) {
        $this->addDynamicDataRetrievalFields($form, $wrapperId, 'calculation', $element_keys, t('Calculating'));
      }
    }
  }

  /**
   * Validates the form submission.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs form submission validation.
   */
  public function validateForm(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission) {
    if ($formState->isSubmitted() && $this->webformHandler->isCalculationEnabled()) {
      $this->getCalculatedData($webformSubmission, $formState, true);
    }
  }

  public function setUsedCalculationToknes(array $calculatedDataTokens) {
    foreach ($calculatedDataTokens as $name => $original) {
      $this->tempUsedCalculationTokens[] = $name;
    }
  }

  public function alterElement(array &$element, FormStateInterface $form_state, array $context) {
    if (isset($element['#webform_key'])) {
      $currentElement = $element['#webform_key'];
      foreach ($this->tempUsedCalculationTokens as $token) {
        $this->usedCalculationTokens[$token][] = $currentElement;
        $this->usedCalculationTokens[$token] = array_unique($this->usedCalculationTokens[$token]);
      }
      $this->tempUsedCalculationTokens = [];
    }
  }

  protected function areOutputsOnThisPage(array $outputs, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission): bool {
    $elementsOnThisPage = $this->getElementsOnThisPage($webform_submission, $form_state);
    foreach ($outputs as $output) {
      if (in_array($output, $elementsOnThisPage)) {
        return TRUE;
      }
      if (in_array($output, $this->tempUsedCalculationTokens)) {
        return TRUE;
      }
      if (isset($this->usedCalculationTokens[$output])) {
        foreach ($this->usedCalculationTokens[$output] as $element) {
          if (in_array($element, $elementsOnThisPage)) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Returns the submitted elements on this webform.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webformSubmission
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param bool $onlyCurrentPage
   * @param bool $includeCurrentPage
   *
   * @return array
   */
  public function getElementsOnThisPage(WebformSubmissionInterface $webformSubmission, FormStateInterface $formState): array {
    $submittedElements = [];
    if ($formState) {
      $pages = $formState->get('pages');
      if ([] !== array_diff(array_keys($pages), ['webform_start', 'webform_preview', 'webform_confirmation'])) {
        // This is a "real" multi page form, e.g. doesn't only consist of webform specific pages.
        $current_page = $formState->get('current_page');
        $validPages = [];
        foreach ($pages as $page => $pageElement) {
          if ($current_page == $page) {
            $validPages[] = $page;
            break;
          }
        }
        $elements = $webformSubmission->getWebform()->getElementsInitializedAndFlattened();
        foreach ($elements as $key => $element) {
          foreach ($validPages as $page) {
            if (isset($element['#webform_parents']) && is_array($element['#webform_parents']) && in_array($page, $element['#webform_parents'])) {
              $submittedElements[$key] = $element;
              break;
            }
          }
        }
      } else {
        // This is a one page webform or has only webform specific pages.
        $submittedElements = $webformSubmission->getWebform()->getElementsInitializedAndFlattened();
      }
    }
    return array_keys($submittedElements);
  }

  protected function doCalculation(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission, bool $force = false) {
    if (!empty($this->configuration['form_processor_enable_calculation'])) {
      $reply = $this->getCalculatedData($webform_submission, $form_state, $force);
      if (empty($reply['is_error'])) {
        foreach ($reply as $field => $value) {
          $this->setElementValue($form, $form_state, $webform_submission, $field, $value);
        }
      }
    }
  }

  protected function getCalculationFields(array $defaultParams): array {
    if (empty($this->calculationFields)) {
      $this->calculationFields = $this->factory->formProcessorCalculationFields($this->getConnection(), $this->getFormProcessor(), $defaultParams, $this->getMetaDataCacheTimeOut());
    }
    return $this->calculationFields;
  }

  protected function getCalculationParams(array $defaultParams): array {
    $calculationFields = $this->getCalculationFields($defaultParams);
    $calculationFields = array_filter($calculationFields, function($field) {
      return empty($field['api.return']);
    });
    return array_map(function ($value) {
      return $value['name'];
    }, $calculationFields);
  }

  protected function getCalculationOutputs(array $defaultParams): array {
    $calculationFields = $this->getCalculationFields($defaultParams);
    $calculationFields = array_filter($calculationFields, function($field) {
      return !empty($field['api.return']);
    });
    return array_map(function ($value) {
      return $value['name'];
    }, $calculationFields);
  }

}

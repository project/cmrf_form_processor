<?php

namespace Drupal\cmrf_form_processor;

use Drupal;
use Drupal\cmrf_form_processor\Plugin\WebformHandler\FormProcessorWebformHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class WebformSubmissionHandler extends FormProcessorBaseHandler {

  /**
   * The webform request handler.
   *
   * @var \Drupal\webform\WebformRequestInterface
   */
  protected $requestHandler;

  /** @var \Drupal\Core\Routing\RouteMatchInterface */
  private $routeMatch;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * @var array
   */
  protected $reply = [];

  /**
   * @var array
   */
  protected $defaultValues = [];

  /**
   * @var bool
   */
  protected $isSubmittedToCiviCRM = false;

  public function __construct(ContainerInterface $container, Factory $factory, FormProcessorWebformHandler $webformHandler) {
    parent::__construct($container, $factory, $webformHandler);
    $this->request = Drupal::service('request_stack')->getCurrentRequest();
    $this->requestHandler = $container->get('webform.request');
    $this->routeMatch = Drupal::routeMatch();
  }

  /**
   * Alter the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs alterations on the form array.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Do not cache this form. We kill the page cache which is quite strict for anonymous users.
    \Drupal::service('page_cache_kill_switch')->trigger();
    $wrapperId = $this->getWrapperId($form_state);
    if (!isset($form['#prefix'])) {
      $form['#prefix'] = '';
    }
    if (!isset($form['#suffix'])) {
      $form['#suffix'] = '';
    }
    $form['#prefix'] .= '<div id="' . $wrapperId . '">';
    $form['#suffix'] = '</div>' . $form['#suffix'];
    $this->webformHandler->isPreparing = false;
  }

  /**
   * Validates the form submission.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs form submission validation.
   */
  public function validateForm(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission) {
    $triggerElement = $formState->getTriggeringElement();
    $updateDataButtons = [
      'cmrf_form_processor_calculation_update_button',
      'cmrf_form_processor_defaults_update_button'
    ];
    if (isset($triggerElement['#name']) && in_array($triggerElement['#name'], $updateDataButtons)) {
      $formState->clearErrors();
      $formState->setRebuild(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   * @param bool $update
   *   (optional) Indicates whether the webform submission is an update or not.
   * @param array $extraParams
   *   (optional) An array used to add extra parameters, for example,
   *   the results of a payment provider.
   *
   * @return void
   *   This method doesn't return a value.
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE, $extraParams = []) {
    // $extraParams might be passed as NULL.
    $extraParams ??= [];
    $state = $webform_submission->getWebform()->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
    if ($this->configuration['states'] && in_array($state, $this->configuration['states'])) {
      $this->sendToCivicrm($webform_submission, $extraParams);
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->updateWebformSubmissionWithSubmittedValues($webform_submission, $form_state);
    $currentPage = null;
    $pages = $form_state->get('pages');
    foreach($pages as $page => $pageElement) {
      if ($form_state->get('current_page') == $page) {
        $currentPage = $page;
      }
    }

    $canSubmitToCiviCRM = false;
    if ($currentPage == '' || $currentPage == WebformInterface::PAGE_CONFIRMATION) {
      $canSubmitToCiviCRM = !$form_state->isRebuilding();
      $state = $webform_submission->getWebform()->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
      if (count($this->configuration['states']) && !in_array($state, $this->configuration['states'])) {
        $canSubmitToCiviCRM = false;
      }
    }

    if ($canSubmitToCiviCRM && strlen($this->configuration['form_processor_redirect_field'])) {
      $this->sendToCivicrm($webform_submission);
      if (isset($this->reply[$this->configuration['form_processor_redirect_field']]) && strlen($this->reply[$this->configuration['form_processor_redirect_field']])) {
        $url = $this->reply[$this->configuration['form_processor_redirect_field']];
        $form_state->setResponse(new Drupal\Core\Routing\TrustedRedirectResponse($url));
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
    if (in_array(WebformSubmissionInterface::STATE_DELETED, $this->configuration['states'])) {
      $this->sendToCivicrm($webform_submission);
    }
  }

  /**
   * Sends the webform submission data to CiviCRM.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   * @param array $extraParams
   *   An array used to add extra parameters, for example,
   *   the results of a payment provider.
   *
   * @return void
   *   This method doesn't return a value.
   * @throws BadRequestException
   */
  public function sendToCivicrm(WebformSubmissionInterface $webform_submission, array $extraParams = []) {
    if (!$this->isSubmittedToCiviCRM) {
      $this->isSubmittedToCiviCRM = TRUE;
      $params = $this->webformSubmissionToApiParams($webform_submission);
      if (is_string($this->configuration['form_processor_original_redirect_field']) && strlen($this->configuration['form_processor_original_redirect_field'])) {
        $extraParams[$this->configuration['form_processor_original_redirect_field']] = $this->getOriginalRedirectUrl($webform_submission);
      }
      $params = array_merge($params, $extraParams);
      $this->reply = $this->factory->api(
        $this->getConnection(),
        'FormProcessor',
        $this->getFormProcessor(),
        $params,
        []
      );
      if (!empty($this->reply['is_error'])) {
        $error = $this->reply['error_message'] ?? '';
        throw new BadRequestException($error);
      }
    }
  }

  /**
   * @return array
   */
  public function getReply(): array {
    return $this->reply;
  }

  /**
   * Returns the original redirect url for this webform.
   *
   * @param WebformSubmissionInterface $webform_submission
   * @return string|null
   */
  private function getOriginalRedirectUrl(WebformSubmissionInterface $webform_submission):? string {
    $webform = $webform_submission->getWebform();
    // Get current route name, parameters, and options.
    $route_name = $this->routeMatch->getRouteName();
    $route_parameters = $this->routeMatch->getRawParameters()->all();
    $route_options = [];

    // Add token route query options.
    if (!$webform->getSetting('confirmation_exclude_token')) {
      $route_options['query']['token'] = $webform_submission->getToken();
    }

    // Add current query to route options.
    if (!$webform->getSetting('confirmation_exclude_query')) {
      $query = $this->request->query->all();
      // Remove Ajax parameters from query.
      unset($query['ajax_form'], $query['_wrapper_format']);
      if ($query) {
        $route_options['query'] = $query;
      }
    }

    $confirmation_type = $this->webformHandler->getWebformSetting('confirmation_type');
    $original_redirect_url = null;
    switch ($confirmation_type) {
      case WebformInterface::CONFIRMATION_PAGE:
        $original_redirect_url = $this->requestHandler->getUrl($webform, $webform_submission, 'webform.confirmation', $route_options);
        break;
      case WebformInterface::CONFIRMATION_URL:
      case WebformInterface::CONFIRMATION_URL_MESSAGE:
        $original_redirect_url = $this->webformHandler->getConfirmationUrl();
        if ($original_redirect_url) {
          $original_redirect_url->mergeOptions($route_options);
        }
        else {
          $route_options['query']['webform_id'] = $webform->id();
          $original_redirect_url = Url::fromRoute($route_name, $route_parameters, $route_options);
        }
        break;
    }
    if ($original_redirect_url) {
      $original_redirect_url->setAbsolute(TRUE);
      $original_redirect_url = $original_redirect_url->toString();
    }
    return $original_redirect_url;
  }


}

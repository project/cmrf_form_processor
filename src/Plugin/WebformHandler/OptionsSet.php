<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor\Plugin\WebformHandler;

use Drupal;
use Drupal\cmrf_core\Entity\CMRFConnector;
use Drupal\webform\Entity\WebformOptions;
use Drupal\cmrf_form_processor\Factory;

class OptionsSet {

  public static function saveOptionsFromFields($fields, $connection, $formProcessorName) {
    $factory = new Factory(Drupal::service('cmrf_core.core'));
    $connectionTitle = $factory->getConnectionTitle($connection);
    $formProcessorTitle = $factory->getFormProcessorTitle($formProcessorName, $connection);
    foreach($fields as $field) {
      $id = static::getOptionSetId($connection, $formProcessorName, $field['name']);
      $option_set = WebformOptions::load($id);
      if ($option_set === null || ($option_set && !count($option_set->getOptions()))) {
        static::saveOptionsFromField($field, $connection, $connectionTitle, $formProcessorName, $formProcessorTitle);
      }
    }
  }

  public static function saveOptionsFromField($field, $connection, $connectionTitle, $formProcessorName, $formProcessorTitle) {
    if (isset($field['options']) && is_array($field['options'])) {
      $id = static::getOptionSetId($connection, $formProcessorName, $field['name']);
      $option_set = WebformOptions::load($id);
      if ($option_set === null) {
        $option_set = WebformOptions::create();
        $option_set->set('id', $id);
      }
      $option_set->set('label', $field['title']);
      $option_set->set('category', t('Form Processor: @fp (@connection)', ['@fp' => $formProcessorTitle, '@connection' => $connectionTitle]));
      $option_set->set('likert', false);
      $option_set->setOptions($field['options']);
      $option_set->save();
    }
  }

  public static function getOptionSetId($connection, $formProcessorName, $fieldName) {
    return 'cmrfformprocessor_'.$connection.'_'.$formProcessorName.'_'.$fieldName;
  }

  public static function flush($force = false) {
    $expires = Drupal::state()->get('cmrfformprocessor.options_set_last_flush_time', 0);
    $expireTime = Drupal::time()->getRequestTime() - (24 * 60 * 60);
    if ($force || ($expires < $expireTime)) {
      $optionSets = WebformOptions::loadMultiple();
      /** @var WebformOptions $optionSet */
      foreach($optionSets as $optionSet) {
        if (str_starts_with($optionSet->id(), 'cmrfformprocessor_')) {
          $optionSet->delete();
        }
      }
      Drupal::state()->set('cmrfformprocessor.options_set_last_flush_time', Drupal::time()->getRequestTime());
    }
  }

  public static function alterOptions(array &$options, array &$element, $options_id = NULL) {
    if (!$options_id || !str_starts_with($options_id, 'cmrfformprocessor_')) {
      return;
    }
    if (!isset($element['#webform'])) {
      return;
    }

    /** @var Drupal\webform\Entity\Webform $webForm */
    $webform = Drupal\webform\Entity\Webform::load($element['#webform']);
    foreach($webform->getHandlers() as $webformHandler) {
      if ($webformHandler instanceof FormProcessorWebformHandler) {
        $defaultParams = $webformHandler->getFormProcessorDefaultParams();
        $connectionId = $webformHandler->getConnection();
        $connectionEntity = CMRFConnector::load($connectionId);
        if ($connectionEntity === null) {
          continue;
        }
        $formProcessor = $webformHandler->getFormProcessor();
        if (str_starts_with($options_id, 'cmrfformprocessor_'.$connectionId.'_'.$formProcessor.'_')) {
          $factory = new Factory(Drupal::service('cmrf_core.core'));
          $fields = $factory->formProcessorFields($connectionId, $formProcessor, $defaultParams);
          $fieldId = substr($options_id, strlen('cmrfformprocessor_' . $connectionId . '_' . $formProcessor . '_'));
          if (isset($fields[$fieldId]) && isset($fields[$fieldId]['options']) && is_array($fields[$fieldId]['options'])) {
            $options = $fields[$fieldId]['options'];
            return;
          }
        }
      }
    }
  }

}

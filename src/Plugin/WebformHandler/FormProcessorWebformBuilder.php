<?php

namespace Drupal\cmrf_form_processor\Plugin\WebformHandler;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\WebformInterface;

/**
 * Provides a Webform builder for the Form Processor.
 */
class FormProcessorWebformBuilder {
  use DependencySerializationTrait;
  /**
   * The Webform object.
   *
   * @var \Drupal\webform\WebformInterface
   */
  public $webform;

  /**
   * FormProcessorWebformBuilder constructor.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The Webform object.
   */
  public function __construct(WebformInterface $webform) {
    $this->webform = $webform;
  }

  /**
   * Adds fields to the Webform.
   *
   * @param array $fields
   *   The fields to be added.
   * @param array $fpValues
   *   The Form Processor values.
   */
  public function addFields($fields, $fpValues, $connection, $formProcessorName, $skipFields=[]) {
    $flattenedElements = $this->webform->getElementsDecodedAndFlattened();
    $flattenedElements = array_merge($flattenedElements, $skipFields);
    $elements = $this->webform->getElementsDecoded();
    foreach ($fields as $key => $value) {
      if ($value == 1 && !array_key_exists($key, $flattenedElements)) {
        if ($fpValues[$key]['type'] == 4) {
          $elements[$key] = [
            '#type' => 'date',
            '#title' => $fpValues[$key]['title'],
          ];
        } elseif (empty($fpValues[$key]['options'])) {
          $elements[$key] = [
            '#type' => 'textfield',
            '#title' => $fpValues[$key]['title'],
          ];
        }
        else {
          $elements[$key] = [
            '#type' => 'select',
            '#title' => $fpValues[$key]['title'],
            '#options' => OptionsSet::getOptionSetId($connection, $formProcessorName, $key),
          ];
        }
      }
    }
    $this->webform->set('elements', Yaml::encode($elements));
    $this->webform->save();
  }

  /**
   * Checks if a field exists in the Webform.
   *
   * @param string $field
   *   The field to check.
   *
   * @return bool
   *   TRUE if the field exists, FALSE otherwise.
   */
  public function isFieldInForm(string $field): bool {
    $elements = $this->webform->getElementsInitializedAndFlattened();
    if (isset($elements[$field])) {
      return TRUE;
    }
    foreach($elements as $key => $element) {
      if ($element['#webform_composite']) {
        foreach ($element['#webform_composite_elements'] as $composite_key => $composite_element) {
          if ($field == $key . '_' . $composite_key) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Deletes fields from the Webform.
   *
   * @param array $fields
   *   The fields to be deleted.
   */
  public function deleteFields($fields) {
    $elements = $this->webform->getElementsDecodedAndFlattened();
    foreach ($fields as $key => $value) {
      if ($value == 0 && array_key_exists($key, $elements)) {
        $this->webform->deleteElement($key);
      }
    }
  }

  public function getFieldTitles(): array {
    $fields = [];
    $elements = $this->webform->getElementsDecodedAndFlattened();
    foreach($elements as $key => $element) {
      $title = $element['#title'];
      if (empty($title)) {
        $title = $key;
      }
      $fields[$key] = $title;
    }
    return $fields;
  }

  /**
   * Saves the Webform.
   */
  public function save() {

  }

}

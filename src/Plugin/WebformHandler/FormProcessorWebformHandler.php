<?php

namespace Drupal\cmrf_form_processor\Plugin\WebformHandler;


use Drupal;
use Drupal\cmrf_form_processor\CalculationHandler;
use Drupal\cmrf_form_processor\DefaultDataHandler;
use Drupal\cmrf_form_processor\ValidationHandler;
use Drupal\cmrf_form_processor\Factory;
use Drupal\cmrf_form_processor\WebformSubmissionHandler;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Plugin\WebformElementInterface;
use Drupal\webform\Utility\WebformOptionsHelper;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission debug handler.
 *
 * @WebformHandler(
 *   id = "cmrf_form_processor",
 *   label = @Translation("CiviCRM Form Processor with CiviMcRestFace (CMRF)"),
 *   category = @Translation("CiviCRM"),
 *   description = @Translation("Post values to CiviCRM form processor"),
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class FormProcessorWebformHandler extends WebformHandlerBase {
  use DependencySerializationTrait;

  /**
   * @var \Drupal\cmrf_form_processor\Factory
   */
  protected $factory;

  /**
   * @var \Drupal\cmrf_form_processor\WebformSubmissionHandler
   */
  protected $webformSubmissionHandler;

  /**
   * @var \Drupal\cmrf_form_processor\DefaultDataHandler
   */
  protected $webformDefaultDataHandler;

  /**
   * @var \Drupal\cmrf_form_processor\ValidationHandler
   */
  protected $webformValidationHandler;

  /**
   * @var \Drupal\cmrf_form_processor\CalculationHandler
   */
  protected $webformCalcuationHandler;

  /**
   * @var \Drupal\webform\Plugin\WebformElementManager
   */
  protected $webformElementManager;

  /**
   * @var ContainerInterface
   */
  protected $container;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->container = $container;
    $instance->factory = new Factory($container->get('cmrf_core.core'));
    $instance->webformElementManager = $container->get('plugin.manager.webform.element');
    $instance->initializeSubHandlers();
    return $instance;
  }

  protected function initializeSubHandlers() {
    if (!$this->container) {
      $this->container = \Drupal::getContainer();
    }
    $this->webformSubmissionHandler = new WebformSubmissionHandler($this->container, $this->factory, $this);
    $this->webformDefaultDataHandler = new DefaultDataHandler($this->container, $this->factory, $this);
    $this->webformValidationHandler = new ValidationHandler($this->container, $this->factory, $this);
    $this->webformCalcuationHandler = new CalculationHandler($this->container, $this->factory, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $t_args = [
      '%plugin_id' => $this->getPluginId(),
      '%connection' => $this->webformSubmissionHandler->getConnection(),
      '%form_processor' => $this->factory->getFormProcessorTitle($this->webformSubmissionHandler->getFormProcessor(), $this->webformSubmissionHandler->getConnection()),
      '%form_processor_params' => $this->configuration['form_processor_params'] ? '[' . implode(', ', $this->configuration['form_processor_params']) . ']' : "[]",
    ];
    return [
      'message' => [
        '#markup' => $this->t('This %plugin_id handler is using connection %connection and form_processor %form_processor with parameters %form_processor_params', $t_args),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'connection' => NULL,
      'states' => [WebformSubmissionInterface::STATE_COMPLETED],
      'form_processor' => NULL,
      'form_processor_params' => [],
      'form_processor_current_contact' => 0,
      'form_processor_background' => 0,
      'form_processor_enable_validation' => 0,
      'form_processor_enable_calculation' => 0,
      'form_processor_enable_default' => 0,
      'form_processor_redirect_field' => '',
      'form_processor_original_redirect_field' => '',
      'metadata_cache_timeout' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    $settings = parent::getSettings();
    if (empty($settings['submission_settings'])) {
      $settings['submission_settings'] = ['fields' => []];
    }
    if (empty($settings['submission_settings']['fields'])) {
      $settings['submission_settings']['fields'] = [];
    }
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    // Backwards compatibility. If form_processor_require_default is present set the enable default to 1
    // If the form_processor_require_default is 1 then set enable default to 2.
    if (!isset($configuration['settings']['form_processor_enable_default']) && isset($configuration['settings']['form_processor_require_default'])) {
      $configuration['settings']['form_processor_enable_default'] = 'enabled';
      if ($configuration['settings']['form_processor_require_default']) {
        $configuration['settings']['form_processor_enable_default'] = 'enabled_and_page_not_found_on_no_data';
      }
      unset($configuration['settings']['form_processor_require_default']);
    }
    if (!isset($configuration['settings']['submission_settings']['fields'])) {
      $configuration['submission_settings']['fields'] = [];
    }
    return parent::setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->applyFormStateToConfiguration($form_state);
    $builder = new FormProcessorWebformBuilder($this->getWebform());

    $selected_connection = empty($this->configuration['connection']) ? NULL : $this->configuration['connection'];
    $selected_formprocessor = empty($this->configuration['form_processor']) ? NULL : $this->configuration['form_processor'];

    $form['fp'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CMRF Form Processor'),
      '#attributes' => ['id' => 'form-processor-stuff'],
    ];
    $connections = $this->factory->getConnectors();
    $form['fp']['connection'] = [
      '#type' => 'select',
      '#title' => $this->t('Connector'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $connections,
      '#required' => TRUE,
      '#default_value' => $this->configuration['connection'],
      '#ajax' => [
        'callback' => [$this, 'connectionCallback'],
        'wrapper' => 'form-processor-stuff',
        'event'   => 'change',
      ],
    ];
    if ($selected_connection) {
      $formProcessors = $this->factory->formProcessorList($selected_connection);
      $form['fp']['form_processor'] = [
        '#type' => 'select',
        '#title' => $this->t('Form Processor'),
        '#empty_option' => $this->t('- None -'),
        '#required' => TRUE,
        '#options' => $formProcessors,
        '#default_value' => $this->configuration['form_processor'],
      ];
      $form['additional'] =
        [
          '#type' => 'fieldset',
          '#title' => $this->t('Form Processor'),
        ];

      // Settings: States.
      $results_disabled = $this->getWebform()->getSetting('results_disabled');
      $form['additional']['states'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Send to CiviCRM'),
        '#options' => [
          WebformSubmissionInterface::STATE_DRAFT_CREATED => $this->t('…when <b>draft is created</b>.'),
          WebformSubmissionInterface::STATE_DRAFT_UPDATED => $this->t('…when <b>draft is updated</b>.'),
          WebformSubmissionInterface::STATE_CONVERTED => $this->t('…when anonymous <b>submission is converted</b> to authenticated.'),
          WebformSubmissionInterface::STATE_COMPLETED => $this->t('…when <b>submission is completed</b>.'),
          WebformSubmissionInterface::STATE_UPDATED => $this->t('…when <b>submission is updated</b>.'),
          WebformSubmissionInterface::STATE_DELETED => $this->t('…when <b>submission is deleted</b>.'),
          WebformSubmissionInterface::STATE_LOCKED => $this->t('…when <b>submission is locked</b>.'),
        ],
        '#access' => $results_disabled ? FALSE : TRUE,
        '#default_value' => $results_disabled ? [WebformSubmissionInterface::STATE_COMPLETED] : $this->configuration['states'],
      ];

      if ($selected_formprocessor) {
        $fpFields = $this->factory->formProcessorFields($selected_connection, $selected_formprocessor, [], $this->webformSubmissionHandler->getMetaDataCacheTimeOut());
        OptionsSet::saveOptionsFromFields($fpFields, $selected_connection, $selected_formprocessor);
        $fpFields = $this->mapTitle($fpFields);
        $fpOutputs = $this->factory->formProcessorOutputs($selected_connection, $selected_formprocessor, [], $this->webformSubmissionHandler->getMetaDataCacheTimeOut());
        $fpOutputs = $this->mapTitle($fpOutputs);
        if (count($fpFields)) {
          $form['additional']['fields'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Fields'),
          ];
          $form['additional']['fields']['update_fields'] = [
            '#type' => 'select',
            '#title' => 'Update fields (this might delete the fields from your form)',
            '#default_value' => $this->configuration['form_processor_update_fields'] ?? 1,
            '#options' => ['0' => $this->t("No"), '1' => $this->t("Yes")],
          ];
          $form['additional']['fields']['help'] = [
            '#type' => 'markup',
            '#markup' => $this->t(
              'With Yes selected, saving this configuration will synchronise the Webform with the Form Processor input fields, adding (not updating) checked fields to and deleting unchecked fields from the Webform.'
            ),
          ];

          // Add a table of form processor fields currently in the form for
          // defining their format to be used for submissions to CiviCRM.
          $form['additional']['submission_settings'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Submission settings'),
            'fields' => [
              '#type' => 'table',
              '#header' => [
                'field' => ['data' => $this->t('Field'), 'width' => '50%'],
                'format' => [
                  'data' => $this->t('Format'),
                  'class' => [RESPONSIVE_PRIORITY_LOW],
                  'width' => '50%',
                ],
              ],
              '#sticky' => TRUE,
            ],
          ];

          foreach ($fpFields as $key => $field) {
            // Add checkboxes for adding/deleting form processor fields to/from
            // the form when saving configuration.
            if (!$this->isFieldCompositeSubField($key)) {
              $form['additional']['fields']['field'][$key] = [
                '#type' => 'checkbox',
                '#title' => $field,
                '#default_value' => $builder->isFieldInForm($key) ? '1' : '0',
                '#states' => [
                  'visible' => [
                    [
                      [':input[name="settings[additional][fields][update_fields]"]' => ['value' => '1']],
                    ],
                  ],
                ],
              ];
            }

            $element = null;
            $webform_element = null;
            if ($this->isFieldCompositeSubField($key)) {
              $element = $this->getCompositeSubElement($key, true);
            } else {
              $element = $this->getWebform()->getElement($key);
            }
            if ($element) {
              $webform_element = $this->webformElementManager->getElementInstance($element);
            }

            // Add configuration option for selecting the format to use for
            // sending field values to CiviCRM.
            if ($webform_element) {
              $submissionOptions = WebformOptionsHelper::appendValueToText($webform_element->getItemFormats());
              $submissionOptions['do_not_submit'] = $this->t('Do not send to CiviCRM');
              $form['additional']['submission_settings']['fields'][$key]['field'] = [
                '#markup' => $element['#title'],
              ];
              $form['additional']['submission_settings']['fields'][$key]['format'] = [
                '#type' => 'select',
                '#title' => $this->t('Item format'),
                '#description' => $this->t('Select how a single value is submitted to CiviCRM.'),
                '#options' => $submissionOptions,
                // Default value for unconfigured elements with options or of a
                // Boolean type should be "Raw value" for sending option keys
                // instead of labels.
                '#default_value' => $this->configuration['submission_settings']['fields'][$key]['format']  ?? $this->getDefaultSubmissionFormatForElement($webform_element),
              ];
            }
          }
        }

        $parameters = $this->factory->formProcessorDefaultsParams($selected_connection, $selected_formprocessor, $this->webformSubmissionHandler->getMetaDataCacheTimeOut());
        $form['additional']['default_params'] =
          [
            '#type' => 'fieldset',
            '#title' => $this->t('Parameters for retrieving default values'),
          ];
        $form['additional']['default_params']['enable_default'] = [
          '#type' => 'select',
          '#title' => $this->t('Enable default values'),
          '#default_value' => $this->configuration['form_processor_enable_default'],
          '#options' => [
            '' => $this->t("No"),
            'enabled' => $this->t("Yes"),
            'enabled_and_page_not_found_on_no_data' => $this->t("Yes and show Page Not Found when no data is retrieved."),
            'enabled_and_access_denied_on_no_data' => $this->t("Yes and show Access Denied when no data is retrieved."),
          ],
          '#description' => $this->t("You can enable retrieval of default data. <br><br>You can also show a page not found or access denied when CiviCRM does not return any data. <br><br>This could be useful when you have a form where a user can edit existing data. When the data does not exists or the user does not have access to it you can the show either page not found or access denied."),
        ];
        foreach ($parameters as $key => $field) {
          $form['additional']['default_params']['param_' . $key] = [
            '#type' => 'select',
            '#title' => $field,
            '#default_value' => $this->configuration['form_processor_params'][$key],
            '#options' => [
              'none' => $this->t('None'),
              'url' => $this->t('URL'),
              'current_user' => $this->t('Current User'),
              'value' => $this->t('Value (you can use tokens)'),
            ],
            '#states' => [
              'invisible' => [
                [
                  [':input[name="settings[additional][default_params][enable_default]"]' => ['value' => '']],
                ],
              ],
            ]
          ];
          $form['additional']['default_params']['value_' . $key] = [
            '#type' => 'textfield',
            '#title' => $field,
            '#default_value' => $this->configuration['form_processor_params_value'][$key],
            '#states' => [
              'invisible' => [
                [
                  [':input[name="settings[additional][default_params][enable_default]"]' => ['value' => '']],
                ],
              ],
              'visible' => [
                ':input[name="settings[additional][params][param_' . $key . ']"]' => ['value' => 'value'],
              ],
              'required' => [
                ':input[name="settings[additional][params][param_' . $key . ']"]' => ['value' => 'value'],
              ],
            ],
          ];
        }
        $form['additional']['default_params']['trigger_defaults'] = [
          '#type' => 'checkboxes',
          '#title' => 'Retrieve defaults again when this field is changed',
          '#default_value' => $this->configuration['form_processor_trigger_defaults'],
          '#options' => $builder->getFieldTitles(),
          '#multiple' => true,
          '#states' => [
            'invisible' => [
              [
                [':input[name="settings[additional][default_params][enable_default]"]' => ['value' => '0']],
              ],
            ],
          ]
        ];
        $form['additional']['params'] =
          [
            '#type' => 'fieldset',
            '#title' => $this->t('Additional configuration'),
          ];
        $form['additional']['params']['enable_validation'] = [
          '#type' => 'select',
          '#title' => 'Enable Validation with the Form Processor',
          '#default_value' => $this->configuration['form_processor_enable_validation'],
          '#options' => [
            0 => "No",
            1 => "Yes",
          ],
        ];
        $form['additional']['params']['enable_calculation'] = [
          '#type' => 'select',
          '#title' => t('Enable Calculations by the Form Processor'),
          '#description' => t('Only possible with CiviCRM Form Processor 2.x and later'),
          '#default_value' => $this->configuration['form_processor_enable_calculation'],
          '#options' => [
            0 => "No",
            1 => "Yes",
          ],
        ];

        $form['additional']['params']['form_processor_redirect_field'] = [
          '#type' => 'select',
          '#title' => t('Enable redirect from the form processor'),
          '#description' => t('Redirect by this field in the form processor. This way CiviCRM can redirect your user to another page. E.g. a payment screen.'),
          '#default_value' => $this->configuration['form_processor_redirect_field'],
          '#options' => ['' => t('Disable redirect from CiviCRM')] + $fpOutputs,
        ];
        $form['additional']['params']['form_processor_original_redirect_field'] = [
          '#type' => 'select',
          '#title' => t('Send the redirect URL to CiviCRM'),
          '#description' => t('This will send the original redirect URL to CiviCRM. Seleect a field if you want to enable this. Enable this if you have a payment processor on your form processor and your payment processor needs redirects back to the confirmation page after the payment is succeeded.'),
          '#default_value' => $this->configuration['form_processor_original_redirect_field'],
          '#options' => ['' => t('Do not send the original redirect URL to CiviCRM')] + $fpFields,
        ];
        if (count($fpFields)) {
          $form['additional']['form_processor_current_contact'] = [
            '#type' => 'select',
            '#title' => 'Fill Current Contact',
            '#default_value' => $this->configuration['form_processor_current_contact'],
            '#options' => [0 => "-None-"] + $fpFields,
          ];
        }
      }
    }

    $form['additional']['metadata_cache_timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Cache Timeout for metadata'),
      '#description' => $this->t('Leave empty to use the hard coded value of: @default_cache_timeout. The metadata cache is used to store information about the fields and options of the form processor.', ['@default_cache_timeout' => $this->factory->getDefaultCache()]),
      '#default_value' => $this->configuration['metadata_cache_timeout'] ?? '',
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * Connection callback for the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form element to render for the connection settings.
   */
  public function connectionCallback(array $form, FormStateInterface $form_state) {
    return $form['settings']['fp'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
    $values = $form_state->getValues();

    // Cleanup states.
    $values['states']                                   = array_values(array_filter($values['states']));
    $this->configuration['metadata_cache_timeout']      = $values['metadata_cache_timeout'];
    $this->configuration['connection']                  = $values['connection'];
    $this->configuration['states']                      = $values['states'];
    $this->configuration['form_processor']              = $values['form_processor'];
    $this->configuration['form_processor_params']       = [];
    $this->configuration['form_processor_params_value'] = [];
    foreach ($values['additional']['default_params'] as $key => $value) {
      if (stripos($key, 'param_') === 0) {
        $this->configuration['form_processor_params'][substr($key, 6)] = $value;
      }
      elseif (stripos($key, 'value_') === 0) {
        $this->configuration['form_processor_params_value'][substr($key, 6)] = $value;
      }
    }
    $this->configuration['form_processor_enable_default'] = $values['additional']['default_params']['enable_default'];
    $this->configuration['form_processor_trigger_defaults'] = $values['additional']['default_params']['trigger_defaults'];
    $this->configuration['form_processor_enable_validation'] = $values['additional']['params']['enable_validation'];
    $this->configuration['form_processor_enable_calculation'] = $values['additional']['params']['enable_calculation'];
    $this->configuration['form_processor_redirect_field'] = $values['form_processor_redirect_field'];
    $this->configuration['form_processor_original_redirect_field'] = $values['form_processor_original_redirect_field'];
    $this->configuration['form_processor_current_contact'] = $values['form_processor_current_contact'];
    $fpFields = $this->factory->formProcessorFields($this->configuration['connection'], $this->configuration['form_processor'], [], $this->webformSubmissionHandler->getMetaDataCacheTimeOut());
    OptionsSet::saveOptionsFromFields($fpFields, $this->configuration['connection'], $this->configuration['form_processor'] );

    if ($values['additional']['fields']['update_fields']) {
      $builder = new FormProcessorWebformBuilder($this->getWebform());
      $skipAddingFields = [];
      if (strlen($this->configuration['form_processor_original_redirect_field'])) {
        $skipAddingFields[] = $this->configuration['form_processor_original_redirect_field'];
      }
      $builder->addFields($values['additional']['fields']['field'], $fpFields, $this->configuration['connection'] , $this->configuration['form_processor'] , $skipAddingFields);
      $builder->deleteFields($values['additional']['fields']['field']);
      if (strlen($this->configuration['form_processor_original_redirect_field'])) {
        $this->getWebform()->deleteElement($this->configuration['form_processor_original_redirect_field']);
      }
      $builder->save();
    }
    $this->configuration['form_processor_update_fields'] = $values['additional']['fields']['update_fields'];
    if (isset($values['additional']['submission_settings']) && count($values['additional']['submission_settings'])) {
      $this->configuration['submission_settings'] = $values['additional']['submission_settings'];
    } else {
      // Load the default submission settings.
      $this->initializeSubHandlers();
      $this->getSubmissionSettings();
    }
  }

  /**
   * @return array
   */
  public function getDefaultValues(): array {
    return $this->webformDefaultDataHandler->getDefaultValues();
  }

  /**
   * Prepare the form.
   * If retrieval of defaults is enabled we should retrieve default values
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param $operation
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function prepareForm(WebformSubmissionInterface $webform_submission, $operation, FormStateInterface $form_state) {
    $this->webformDefaultDataHandler->prepareForm($webform_submission, $operation, $form_state);
  }

  /**
   * Alter the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs alterations on the form array.
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->webformDefaultDataHandler->alterForm($form, $form_state, $webform_submission);
    if ($this->isCalculationEnabled()) {
      $this->webformCalcuationHandler->alterForm($form, $form_state, $webform_submission);
    }
    $this->webformSubmissionHandler->alterForm($form, $form_state, $webform_submission);
  }

  /**
   * Validates the form submission.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs form submission validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->webformSubmissionHandler->validateForm($form, $form_state, $webform_submission);
    if ($this->isCalculationEnabled()) {
      $this->webformCalcuationHandler->validateForm($form, $form_state, $webform_submission);
    }
    $this->webformValidationHandler->validateForm($form, $form_state, $webform_submission);
  }

  public function isCalculationEnabled(): bool {
    return !empty($this->configuration['form_processor_enable_calculation']);
  }

  /**Returns the name of the connection.
   *
   * @return string
   */
  public function getConnection(): string {
    return $this->webformSubmissionHandler->getConnection();
  }

  /**
   * Returns the name of the form processor
   *
   * @return string
   */
  public function getFormProcessor(): string {
    return $this->webformSubmissionHandler->getFormProcessor();
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   * @param bool $update
   *   (optional) Indicates whether the webform submission is an update or not.
   * @param array $extraParams
   *   (optional) An array used to add extra parameters, for example,
   *   the results of a payment provider.
   *
   * @return void
   *   This method doesn't return a value.
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE, $extraParams = []) {
    $this->webformSubmissionHandler->postSave($webform_submission, $update, $extraParams);
  }

  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->webformSubmissionHandler->submitForm($form, $form_state, $webform_submission);
  }

  /**
   * {@inheritDoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
    $this->webformSubmissionHandler->postDelete($webform_submission);
  }

  /* ************************************************************************ */
  // Webform methods.
  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function alterElement(array &$element, FormStateInterface $form_state, array $context) {
    $this->webformCalcuationHandler->alterElement($element, $form_state, $context);
  }

  public function getReply(): array {
    return $this->webformSubmissionHandler->getReply();
  }

  public function getCalculatedData(WebformSubmissionInterface $webformSubmission, bool $force = false): array {
    return $this->webformCalcuationHandler->getCalculatedData($webformSubmission,  null, $force);
  }

  public function setUsedCalculationToknes(array $calculatedDataTokens) {
    $this->webformCalcuationHandler->setUsedCalculationToknes($calculatedDataTokens);
  }

  public function saveOptionsFromFields() {
    $selected_formprocessor = $this->getFormProcessor();
    $selected_connection = $this->getConnection();
    if ($selected_formprocessor) {
      $fpFields = $this->factory->formProcessorFields($selected_connection, $selected_formprocessor, [], $this->webformSubmissionHandler->getMetaDataCacheTimeOut());
      OptionsSet::saveOptionsFromFields($fpFields, $selected_connection, $selected_formprocessor);
    }
  }

  /**
   * Get a webform submission's webform setting.
   *
   * @param string $name
   *   Setting name.
   * @param null|mixed $default_value
   *   Default value.
   *
   * @return mixed
   *   A webform setting.
   */
  public function getWebformSetting($name, $default_value = NULL) {
    $value = $this->getWebform()->getSetting($name, $default_value);
    if ($value !== NULL) {
      return $this->tokenManager->replace($value, $this->getWebformSubmission());
    }
    else {
      return $default_value;
    }
  }

  /**
   * Get the webform's confirmation URL.
   *
   * @return \Drupal\Core\Url|false
   *   The url object, or FALSE if the path is not valid.
   *
   * @see \Drupal\Core\Path\PathValidatorInterface::getUrlIfValid
   */
  public function getConfirmationUrl():? Url {
    $confirmation_url = trim($this->getWebformSetting('confirmation_url', ''));

    $request = Drupal::request();
    if (strpos($confirmation_url, '/') === 0) {
      // Get redirect URL using an absolute URL for the absolute  path.
      $redirect_url = Url::fromUri($request->getSchemeAndHttpHost() . $confirmation_url);
    }
    elseif (preg_match('#^[a-z]+(?:://|:)#', $confirmation_url)) {
      // Get redirect URL from URI (i.e. http://, https:// or ftp://)
      // and Drupal custom URIs (i.e internal:).
      $redirect_url = Url::fromUri($confirmation_url);
    }
    elseif (strpos($confirmation_url, '<') === 0) {
      // Get redirect URL from special paths: '<front>' and '<none>'.
      $redirect_url = $this->pathValidator->getUrlIfValid($confirmation_url);
    }
    else {
      // Get redirect URL by validating the Drupal relative path which does not
      // begin with a forward slash (/).
      $confirmation_url = $this->aliasManager->getPathByAlias('/' . $confirmation_url);
      $redirect_url = $this->pathValidator->getUrlIfValid($confirmation_url);
    }
    return $redirect_url;
  }

  /**
   * Map the titles from the values array.
   *
   * @param array $values
   *   The array of values to extract titles from.
   *
   * @return array
   *   The array of titles extracted from the values.
   */
  private function mapTitle($values) {
    return array_map(function ($value) {
      return $value['title'];
    }, $values);
  }

  /**
   * Returns an array with default params.
   *
   * @return array
   */
  public function getFormProcessorDefaultParams(): array {
    return $this->webformDefaultDataHandler->getFormProcessorDefaultParams();
  }

  /**
   * Return the submission settings. Which is an array containing the field name,
   * and the submission format of the element.
   * @return array
   */
  public function getSubmissionSettings(): array {
    if (isset($this->configuration['submission_settings']['fields']) && is_string($this->configuration['submission_settings']['fields'])) {
      unset($this->configuration['submission_settings']['fields']);
    }
    if (!isset($this->configuration['submission_settings']['fields'])) {
      $this->configuration['submission_settings']['fields'] = [];
      $fpFields = $this->factory->formProcessorFields($this->getConnection(), $this->getFormProcessor(), [], $this->webformSubmissionHandler->getMetaDataCacheTimeOut());
      $builder = new FormProcessorWebformBuilder($this->getWebform());
      foreach ($fpFields as $key => $field) {
        if ($builder->isFieldInForm($key) && $element = $this->getWebform()->getElement($key)) {
          $this->configuration['submission_settings']['fields'][$key]['format'] = null;
          try {
            $webform_element = $this->webformElementManager->getElementInstance($element);
            $format = $this->getDefaultSubmissionFormatForElement($webform_element);
            $this->configuration['submission_settings']['fields'][$key]['format'] = $format;
          }
          catch (\Exception $e) {
          }
        }
      }
    }
    return $this->configuration['submission_settings']['fields'];
  }

  /**
   * @param \Drupal\webform\Plugin\WebformElementInterface $webform_element
   *
   * @return string|null
   */
  public function getDefaultSubmissionFormatForElement(WebformElementInterface $webform_element):? string {
    if ($webform_element->hasProperty('options')) {
      return 'raw';
    }
    if ($webform_element instanceof Drupal\webform\Plugin\WebformElement\BooleanBase) {
      return 'raw';
    }
    if ($webform_element instanceof Drupal\webform\Plugin\WebformElement\DateBase) {
      return 'html_date';
    }
    if ($webform_element instanceof Drupal\webform\Plugin\WebformElement\WebformManagedFileBase) {
      return 'url';
    }
    return array_key_first($webform_element->getItemFormats());
  }

  public function isFieldCompositeSubField(string $field): bool {
    $elements = $this->webform->getElementsInitializedAndFlattened();
    if (isset($elements[$field]) && !$elements[$field]['#webform_composite']) {
      return FALSE;
    }
    foreach($elements as $key => $element) {
      if ($element['#webform_composite']) {
        foreach ($element['#webform_composite_elements'] as $composite_key => $composite_element) {
          if ($field == $key . '_' . $composite_key) {
            return TRUE;
          }
        }
      }
    }
    return FALSE;
  }

  public function isFieldComposite(string $field): bool {
    $elements = $this->webform->getElementsInitializedAndFlattened();
    if (isset($elements[$field]) && $elements[$field]['#webform_composite']) {
      return TRUE;
    }
    return FALSE;
  }

  public function getCompositeSubElement(string $field, bool $includeParentTitle):? array {
    $elements = $this->webform->getElementsInitializedAndFlattened();
    if (isset($elements[$field]) && !$elements[$field]['#webform_composite']) {
      return NULL;
    }
    foreach($elements as $key => $element) {
      if ($element['#webform_composite']) {
        foreach ($element['#webform_composite_elements'] as $composite_key => $composite_element) {
          if ($field == $key . '_' . $composite_key) {
            if ($includeParentTitle) {
              $composite_element['#title'] = $element['#title'] . ' ' . $composite_element['#title'];
            }
            return $composite_element;
          }
        }
      }
    }
    return null;
  }
}

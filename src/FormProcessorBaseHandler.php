<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor;

use \Drupal;
use Drupal\cmrf_form_processor\Plugin\WebformHandler\FormProcessorWebformHandler;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\webform\Plugin\WebformElement\DateBase;
use Drupal\webform\Plugin\WebformElementAttachmentInterface;
use Drupal\webform\Plugin\WebformElementOtherInterface;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class FormProcessorBaseHandler {
  use DependencySerializationTrait;

  /**
   * @var \Drupal\cmrf_form_processor\Plugin\WebformHandler\FormProcessorWebformHandler
   */
  protected $webformHandler;

  /**
   * @var \Drupal\cmrf_form_processor\Factory
   */
  protected $factory;

  /**
   * @var array
   */
  protected $configuration = [];

  /**
   * @var String
   */
  protected $wrapperId;

  /**
   * @var \Drupal\webform\Plugin\WebformElementManager
   */
  protected $webformElementManager;

  /**
   * The Webform submission exporter.
   *
   * @var \Drupal\webform\WebformSubmissionExporterInterface
   */
  protected $submissionExporter;

  public function __construct(ContainerInterface $container, Factory $factory, FormProcessorWebformHandler $webformHandler) {
    $this->factory = $factory;
    $this->webformHandler = $webformHandler;
    $this->configuration = $webformHandler->getSettings();
    $this->webformElementManager = $container->get('plugin.manager.webform.element');
    $this->submissionExporter = $container->get('webform_submission.exporter');
  }

  /**
   * Returns the name of the connection
   *
   * @return string
   */
  public function getConnection(): string {
    return $this->configuration['connection'];
  }

  /**
   * Returns the name of the form processor
   *
   * @return string
   */
  public function getFormProcessor(): string {
    return $this->configuration['form_processor'];
  }

  /**
   * @return string
   */
  public function getMetaDataCacheTimeOut(): string {
    return $this->configuration['metadata_cache_timeout'] ?? $this->factory->getDefaultCache();
  }

  /**
   * Get the contact ID of the current user.
   *
   * @return int|null
   *   The contact ID of the current user, or null if not available.
   */
  protected function getContactId() {
    $id = Drupal::currentUser()->id();
    $currentUser = Drupal::entityTypeManager()->getStorage('user')->load($id);
    return $currentUser->field_user_contact_id->value ?? NULL;
  }

  protected function getWrapperId(FormStateInterface $form_state): string {
    if (empty($this->wrapperId)) {
      $this->wrapperId = $form_state->getFormObject()->getFormId() . '_cmrf_form_processor';
    }
    return $this->wrapperId;
  }

  /**
   * Update the webform submission with the submitted data
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  protected function updateWebformSubmissionWithSubmittedValues(WebformSubmissionInterface $webform_submission, FormStateInterface $form_state) {
    if (count($form_state->getErrors())) {
      return;
    }
    $data = $webform_submission->getData();
    $submittedData = $form_state->getUserInput();
    foreach($data as $key => $value) {
      if (isset($submittedData[$key])) {
        $submittedValue = $submittedData[$key];
        $element = $this->webformHandler->getWebform()->getElement($key);
        if ($element) {
          $webformElement = $this->webformElementManager->getElementInstance($element);
          //  Other elements are elements that allow a user to select from a list
          //  of default values, but of the value they want is not in the list they
          //  can fill in another element. These values are shown as two values
          //  in the form but must be flattened into on value for processing
          //  the code below checks if an element a 'Other' element and flattens
          //  the value.
          if ($webformElement instanceof WebformElementOtherInterface) {
            $type = $this->getOptionsOtherType($webformElement);
            if (is_array($submittedValue) && $submittedValue[$type] == '_other_') {
              $submittedValue = $submittedValue['other'];
            } elseif (is_array($submittedValue)) {
              $submittedValue = $submittedValue[$type];
            }
          } elseif ($webformElement instanceof DateBase) {
            if (is_array($submittedValue)) {
              $submittedValue = date('c', strtotime($submittedValue['date'] . ' ' . $submittedValue['time']));
            }
          }
        }
        $data[$key] = $submittedValue;
      }
    }
    $webform_submission->setData($data);
  }

  /**
   * Get the other option base element type.
   *
   * @return string|null
   *   The base element type (select|radios|checkboxes|buttons).
   */
  protected function getOptionsOtherType(Drupal\webform\Plugin\WebformElementInterface $webformElement) {
    if (preg_match('/webform_(select|radios|checkboxes|buttons)_other$/', $webformElement->getPluginId(), $match)) {
      return $match[1];
    }
    return null;
  }

  /**
   * Returns the submitted elements on this webform.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webformSubmission
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param bool $onlyCurrentPage
   * @param bool $includeCurrentPage
   *
   * @return array
   */
  public function getSubmittedElements(WebformSubmissionInterface $webformSubmission, FormStateInterface $formState, bool $includePreviousPages, bool $includeCurrentPage = TRUE): array {
    $submittedElements = [];
    if ($formState) {
      $pages = $formState->get('pages');
      if (!is_array($pages)) {
        $pages = [];
      }
      if ([] !== array_diff(array_keys($pages), ['webform_start', 'webform_preview', 'webform_confirmation'])) {
        // This is a "real" multi page form, e.g. doesn't only consist of webform specific pages.
        $current_page = $formState->get('current_page');
        $validPages = [];
        foreach ($pages as $page => $pageElement) {
          if ($current_page == $page) {
            if ($includeCurrentPage) {
              $validPages[] = $page;
            }
            break;
          }
          if ($includePreviousPages) {
            $validPages[] = $page;
          }
        }
        $elements = $webformSubmission->getWebform()->getElementsInitializedFlattenedAndHasValue();
        foreach ($elements as $key => $element) {
          foreach ($validPages as $page) {
            if (isset($element['#webform_parents']) && is_array($element['#webform_parents']) && in_array($page, $element['#webform_parents'])) {
              $submittedElements[$key] = $element;
              break;
            }
            if (empty($element['#webform_parent_key'])) {
              // This element is not on a subpage. Usually this are hidden fields on top of the webform configration.
              $submittedElements[$key] = $element;
              break;
            }
          }
        }
      } else {
        // This is a one page webform or has only webform specific pages.
        $submittedElements = $webformSubmission->getWebform()->getElementsInitializedFlattenedAndHasValue();
      }
    }

    // Values of cmrf_display fields aren't submitted.
    $submittedElements = array_filter($submittedElements, fn (array $element) => $element['#type'] !== 'cmrf_display');

    return array_keys($submittedElements);
  }

  /**
   * @param FormStateInterface $formState
   * @return bool
   */
  public function isMultiPageForm(FormStateInterface $formState): bool {
    $current_page = $formState->get('current_page');
    if ($current_page !== "") {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param FormStateInterface $formState
   * @return int|NULL
   */
  public function getPageNumber(FormStateInterface $formState):? int {
    $pages = $formState->get('pages');
    $current_page = $formState->get('current_page');
    $currentPageNr = 0;
    foreach ($pages as $page => $pageElement) {
      if ($current_page == $page) {
        return $currentPageNr;
      }
      $currentPageNr++;
    }
    return NULL;
  }

  /**
   * Convert a webform submission to API parameters.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return array
   *   The API parameters derived from the webform submission.
   */
  public function webformSubmissionToApiParams(WebformSubmissionInterface $webform_submission, FormStateInterface $form_state = null): array {
    $params = [];
    $data = $webform_submission->getData();
    if ($form_state) {
      $validElements = $this->getSubmittedElements($webform_submission, $form_state, TRUE);
      $data = array_filter($data, function($v, $k) use ($validElements) {
        return in_array($k, $validElements);
      }, ARRAY_FILTER_USE_BOTH);
    }
    $fields = $this->webformHandler->getSubmissionSettings();
    foreach ($data as $key => $value) {
      if ($this->webformHandler->isFieldComposite($key) && is_array($value)) {
        foreach ($value as $sub_key => $sub_value) {
          $composite_key = $key . '_' . $sub_key;
          $format = $fields[$composite_key]['format'] ?? NULL;
          if ($format === 'do_not_submit') {
            continue;
          }
          $element = $this->webformHandler->getCompositeSubElement($composite_key, false);
          if ($element) {
            $element['#value'] = $sub_value;
            $params[$composite_key] = $this->formatSubmissionValue($sub_value, $format, $element, $webform_submission, $sub_key);
          }
        }
      } else {
        $format = $fields[$key]['format'] ?? NULL;
        if ($format === 'do_not_submit') {
          continue;
        }
        $element = $this->webformHandler->getWebform()->getElement($key);
        if ($element) {
          $params[$key] = $this->formatSubmissionValue($value, $format, $element, $webform_submission);
        }
      }
    }
    if ($this->configuration['form_processor_current_contact']) {
      $params[$this->configuration['form_processor_current_contact']] = $this->getContactId();
    }
    return $params;
  }

  protected function formatSubmissionValue($value, $format, array $element, WebformSubmissionInterface $webformSubmission, string $composite_key = NULL) {
    $webformElement = $this->webformElementManager->getElementInstance($element);
    if ($format === null) {
      $format = $this->webformHandler->getDefaultSubmissionFormatForElement($webformElement);
    }
    if ($format && $format != 'raw') {
      // Use the selected format from configuration for rendering the field
      // value. We're using Webform's buildExportRecord() method for
      // rendering, as it takes care of element-specific formatting, such as
      // deciding whether to render as HTML or text.
      $element['#format'] = $format;
      if ($webformElement instanceof Drupal\webform\Plugin\WebformElement\WebformManagedFileBase) {
        $fid = $value['fids'] ?? $value;
        $value = NULL;
        if (!empty($fid)) {
          $file = File::load($fid);
          if ($format == "url") {
            $value = $file->createFileUrl(FALSE);
          }
          else {
            $fileData = [
              'name' => $file->getFilename(),
              'mime_type' => $file->getMimeType(),
              'content' => base64_encode(file_get_contents($file->getFileUri())),
            ];
            $value = $fileData;
          }
        }
      } elseif ($webformElement instanceof DateBase) {
        // Make sure we export date format according to the display values.
        $value = (string) $webformElement->formatText($element, $webformSubmission, ['webform' => $this->webformHandler->getWebform()]);
      } else {
        $export_options = $this->submissionExporter->getDefaultExportOptions();
        $export_options['webform'] = $this->webformHandler->getWebform();
        if ($webformElement instanceof WebformElementOtherInterface) {
          $type = $this->getOptionsOtherType($webformElement);
          if (is_array($value) && $value[$type] == '_other_') {
            $export_options['delta'] = 'other';
          } elseif (is_array($value)) {
            $export_options['delta'] = $type;
          }
        }
        $export_record = $webformElement->buildExportRecord($element, $webformSubmission, $export_options);
        $value = (string) reset($export_record);
      }
    }
    return $value;
  }

  /**
   * Add the functionality for retrieving default data when a field is changed.
   * Or for calculation.
   *
   * @param array $form
   * @param string $wrapperId
   * @param string $type
   * @param array $triggers
   * @param string $processMessage
   *
   * @return void
   */
  protected function addDynamicDataRetrievalFields(array &$form, string $wrapperId, string $type, array $triggers, string $processMessage) {
    $form['cmrf_form_processor_' . $type] = [
      '#prefix' => '<div class="js-webform-form_processor_' . $type . '" data-webform-element-keys="' . implode(',', $triggers) . '">',
      '#suffix' => '</div>',
      'update' => [
        '#type' => 'submit',
        '#value' => t('Retrieve'),
        '#ajax' => [
          'wrapper' => $wrapperId,
          'method' => 'replaceWith',
          'progress' => [
            'type' => 'throbber',
            'message' => $processMessage,
          ],
        ],
        // Disable validation, hide button, add submit button trigger class.
        '#attributes' => [
          'formnovalidate' => 'formnovalidate',
          'class' => [
            'use-ajax-submit',
            'js-hide',
            'js-webform-computed-submit',
          ],
        ],
        '#name' => 'cmrf_form_processor_' . $type . '_update_button',
      ],
      '#attached' => ['library' => ['cmrf_form_processor/webform.element.form_processor_' . $type]],
    ];
  }

  /**
   * Returns an array with default params.
   *
   * @return array
   */
  protected function getFormProcessorSubmissionParams(): array {
    $params = [];
    $form_processor_params = $this->configuration['form_processor_params'];
    $form_processor_params_value = $this->configuration['form_processor_params_value'] ?? [];
    foreach ($form_processor_params as $key => $fp_param) {
      if ($fp_param == 'url' && Drupal::request()->get($key)) {
        $params[$key] = Drupal::request()->get($key);
      }
      if ($fp_param == 'current_user' && $this->getContactId()) {
        $params[$key] = $this->getContactId();
      }
      if ($fp_param == 'value' && !empty($form_processor_params_value[$key])) {
        $parsedValue = Drupal::token()->replace($form_processor_params_value[$key]);
        if (!empty($parsedValue)) {
          $params[$key] = $parsedValue;
        }
      }
    }
    return $params;
  }

  /**
   * Set the value on an element.
   * This function also retrieves and stores a file and formats date time values.
   */
  protected function setElementValue(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission, string $field, $value): void {
    $fields = $this->configuration['submission_settings']['fields'] ?? [];
    $element =& WebformElementHelper::getElement($form, $field);
    $originalElement = $this->webformHandler->getWebform()->getElement($field);
    if ($element) {
      $elementInstance = $this->webformElementManager->getElementInstance($element);
      if ($elementInstance instanceof WebformElementAttachmentInterface) {
        if (is_string($value) && strlen($value)) {
          $fileHandler = FileHandler::getInstance();
          $subPath = $webformSubmission->getWebform()->id().'/'.$field;
          $fid = $fileHandler->createFileFromUrl($value, $subPath);
          $element['#value'] = [$fid];
          if (isset($element['#default_value'])) {
            $element['#default_value'] = $element['#value'];
          }
        }
      } elseif ($elementInstance instanceof DateBase) {
        try {
          $inputFormat = \DateTime::ATOM;
          $htmlDateFormat = 'Y-m-d';
          /** @var \Drupal\Core\Datetime\DateFormatInterface $date_format_entity */
          if ($date_format_entity = DateFormat::load('html_date')) {
            $htmlDateFormat = $date_format_entity->getPattern();
          }
          if ($elementInstance instanceof Drupal\webform\Plugin\WebformElement\DateTime) {
            /** @var \Drupal\Core\Datetime\DateFormatInterface $time_format_entity */
            $htmlTimeFormat = 'H:i';
            if ($time_format_entity = DateFormat::load('html_time')) {
              $htmlTimeFormat .= $time_format_entity->getPattern();
            }
            $htmlDateFormat .= ' ' . $htmlTimeFormat;
          }
          if (isset($fields[$field]['format']) && $fields[$field]['format'] != 'raw') {
            $originalElement['#format'] = $fields[$field]['format'];
            $elementFormat = $elementInstance->getItemFormat($originalElement);
            /** @var \Drupal\Core\Datetime\DateFormatInterface $date_format_entity */
            if ($date_format_entity = DateFormat::load($elementFormat)) {
              $inputFormat = $date_format_entity->getPattern();
            }
          }
          $dateTime = DrupalDateTime::createFromFormat($inputFormat, $value);
          $element['#value'] = $dateTime->format($htmlDateFormat);
          if (isset($element['#default_value'])) {
            $element['#default_value'] = $element['#value'];
          }
        } catch (\Throwable $ex) {
          // is it something else than a date ignore
          $element['#value'] = $value;
          if (isset($element['#default_value'])) {
            $element['#default_value'] = $element['#value'];
          }
        }
      }
      elseif ($value !== NULL) {
        $element['#value'] = $value;
        if (isset($element['#default_value'])) {
          $element['#default_value'] = $element['#value'];
        }
      }
    }
  }

}

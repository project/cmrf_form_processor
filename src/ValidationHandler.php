<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Drupal\cmrf_form_processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

class ValidationHandler extends FormProcessorBaseHandler {

  /**
   * Validates the form submission.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission object.
   *
   * @return void
   *   This method performs form submission validation.
   */
  public function validateForm(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission) {
    if ($formState->isSubmitted() && $this->webformHandler->isCalculationEnabled()) {
      $calculatedData = $this->webformHandler->getCalculatedData($webformSubmission);
      if (!empty($calculatedData['is_error']) && isset($calculatedData['validation_errors'])) {
        $this->setValidationErrorMessage($calculatedData['validation_errors'], $form, $formState, $webformSubmission);
      }
    }
    if (!count($formState->getErrors()) && !$formState->isRebuilding() && !empty($this->configuration['form_processor_enable_validation'])) {
      // Clone the webform submission because it is only used to hold the submitted values as well.
      $tempSubmission = clone $webformSubmission;
      $this->updateWebformSubmissionWithSubmittedValues($tempSubmission, $formState);
      $params = $this->webformSubmissionToApiParams($tempSubmission, $formState);
      $reply = $this->factory->api(
        $this->getConnection(),
        'FormProcessorValidation',
        $this->getFormProcessor(),
        $params,
        []
      );
      unset($reply['is_error']);
      $this->setValidationErrorMessage($reply, $form, $formState, $tempSubmission);
    }
  }

  public function setValidationErrorMessage(array $errors, array $form, FormStateInterface $formState, WebformSubmissionInterface $webformSubmission): void {
    $elementsOnThisPage = $this->getSubmittedElements($webformSubmission, $formState, FALSE);
    foreach ($errors as $field => $msg) {
      if (in_array($field, $elementsOnThisPage) || $formState->get('current_page') == WebformInterface::PAGE_CONFIRMATION) {
        $element = $this->getElement($field, $form);
        if (is_array($element) && isset($element['#parents'])) {
          $formState->setError($element, $msg);
        } else {
          $formState->setErrorByName($field, $msg);
        }
      }
    }
  }

  private function getElement(string $key, array $form):? array {
    foreach($form as $id => $element) {
      if (strpos($id, '#') !== 0) {
        if ($id == $key) {
          return $element;
        } elseif (is_array($element)) {
          $e = $this->getElement($key, $element);
          if (is_array($e)) {
            return $e;
          }
        }
      }
    }
    return null;
  }

}

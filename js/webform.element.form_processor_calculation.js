/**
 * @file
 * JavaScript behaviors for computed elements.
 */

(function ($, Drupal, once) {

  'use strict';

  Drupal.webform = Drupal.webform || {};
  Drupal.webform.formProcessorCalculation = Drupal.webform.formProcessorCalculation || {};
  Drupal.webform.formProcessorCalculation.delay = Drupal.webform.formProcessorCalculation.delay || 3000;

  var computedElements = [];

  /**
   * Initialize computed elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformFormProcessorCalculation = {
    attach: function (context) {
      // Find computed elements and build trigger selectors.
      $(once('webform-form_processor_calculation', '.js-webform-form_processor_calculation', context)).each(function () {
        // Get computed element and form.
        var $element = $(this);
        var $form = $element.closest('form');

        // Get unique id for computed element based on the element name
        // and form id.
        var id = $form.attr('id') + '-' + $element.find('input[type="hidden"]').attr('name');

        // Get elements that are used by the computed element.
        var elementKeys = $(this).data('webform-element-keys').split(',');
        if (!elementKeys) {
          return;
        }

        // Get computed element trigger selectors.
        var inputs = [];
        var inputsWithError = [];
        $.each(elementKeys, function (i, key) {
          // Exact input match.
          inputs.push('[name="' + key + '"]');
          // Sub inputs. (aka #tree)
          inputs.push('[name^="' + key + '["]');

          // Exact input match.
          inputsWithError.push('[name="' + key + '"].error');
          // Sub inputs. (aka #tree)
          inputsWithError.push('[name^="' + key + '["].error');
        });
        var triggers = inputs.join(',');

        // Track computed elements.
        computedElements.push({
          id: id,
          element: $element,
          form: $form,
          triggers: triggers,
        });

        // Clear computed last values to ensure that a computed element is
        // always re-computed on page load.
        $element.attr('data-webform-form_processor_calculation-last', '');
        let doScroll = true;
        if (inputsWithError.length > 0) {
          const triggersWithErrorSelector = inputsWithError.join(',');
          if ($(context).find(triggersWithErrorSelector).length > 0) {
            $(context).find(triggersWithErrorSelector)[0].focus();
            const length = $(context).find(triggersWithErrorSelector)[0].value.length;
            $(context).find(triggersWithErrorSelector)[0].setSelectionRange(length, length);
            doScroll = false;
          }
        }
        if (doScroll) {
          const height = localStorage.getItem('webform-form_processor_calculation-scroll-height');
          if (height) {
            $(window).scrollTop(height);
          }
          localStorage.removeItem('webform-form_processor_calculation-scroll-height');
        }
      });

      // Initialize triggers for each computed element.
      $.each(computedElements, function (index, computedElement) {
        // Get trigger from the current context.
        var $triggers = $(context).find(computedElement.triggers);
        // Make sure current context has triggers.
        if (!$triggers.length) {
          return;
        }

        // Make sure triggers are within the computed element's form
        // and only initialized once.
        $triggers = $(once('webform-form_processor_calculation-triggers-' + computedElement.id, computedElement.form.find($triggers)));
        // Double check that there are triggers which need to be initialized.
        if (!$triggers.length) {
          return;
        }

        computedElement.element.attr('data-webform-form_processor_calculation-last', $triggers.serialize());
        initializeTriggers(computedElement.element, $triggers);
      });

      /**
       * Initialize computed element triggers.
       *
       * @param {jQuery} $element
       *   An jQuery object containing the computed element.
       * @param {jQuery} $triggers
       *   An jQuery object containing the computed element triggers.
       */
      function initializeTriggers($element, $triggers) {
        // Add event handler to computed element triggers.
        $triggers.on('change', triggerUpdate);
        $triggers.on('keyup', queueUpdate);

        // Add event handler to computed element tabledrag.
        var $draggable = $triggers.closest('tr.draggable');
        if ($draggable.length) {
          $draggable.find('.tabledrag-handle').on('mouseup pointerup touchend', queueUpdate);
        }

        // Queue computed element updates using a timer.
        var timer = null;
        function queueUpdate() {
          if (timer) {
            window.clearTimeout(timer);
            timer = null;
          }
          timer = window.setTimeout(triggerUpdate, Drupal.webform.formProcessorCalculation.delay);
        }

        function triggerUpdate() {
          if (timer) {
            window.clearTimeout(timer);
            timer = null;
          }
          // Get computed element wrapper.
          var $wrapper = $element.find('.js-webform-form_processor_calculation-wrapper');

          // If computed element is loading, requeue the update and wait for
          // the computed element to be updated.
          if ($wrapper.hasClass('webform-form_processor_calculation-loading')) {
            queueUpdate();
            return;
          }

          // Prevent duplicate computations.
          // @see Drupal.behaviors.formSingleSubmit
          var formValues = $triggers.serialize();
          var previousValues = $element.attr('data-webform-form_processor_calculation-last');
          if (previousValues === formValues) {
            return;
          }
          $element.attr('data-webform-form_processor_calculation-last', formValues);

          // Add loading class to computed wrapper.
          $wrapper.addClass('webform-form_processor_calculation-loading');
          localStorage.setItem('webform-form_processor_calculation-scroll-height', $(window).scrollTop());
          $('.form-wrapper').hide();

          // Trigger computation.
          $element.find('.js-form-submit').trigger('mousedown');
        }
      }
    }
  };

})(jQuery, Drupal, once);
